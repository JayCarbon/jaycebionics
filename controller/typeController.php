<?php
$action=filter_var($_GET["action"]??"type",FILTER_SANITIZE_FULL_SPECIAL_CHARS);

switch ($action){
case "type":
    $objectTypeManager=new TypeManager($lePDO);
    $types=$objectTypeManager->fetchAllType();
    require("view/type/gererType.php");
    break;


case "createType":
    require("view/type/createType.php");
    break;


case "createTypeTraitement":
    $nomType=filter_var($_POST["nomType"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $id=filter_var($_POST["id"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $objectTypeManager = new TypeManager($lePDO);
    $objectTypeManager->createType($nomType,$id);
    if($objectTypeManager==true){
        $_SESSION["validation"]="Création du type de prothese effectuée";
        header("location:?path=type&action=type");
    }else{
        $_SESSION["erreur"]="La création du type de prothese n'a pas fonctionée";
        header("location:?path=type&action=type");
    }
    break;


case "updateType":
    $id=filter_var($_GET["id"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $nomType=filter_var($_GET["nomType"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $objectTypeManager= new TypeManager($lePDO);
    $type=$objectTypeManager->fetchTypeById($id);
    require("view/type/updateType.php");
    break;

    
case "traitementUpdateType":
    $id=filter_var($_POST["id"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $nomType=filter_var($_POST["nomType"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
    $objectTypeManager=new TypeManager($lePDO);
    $objectTypeManager->updateType($id, $nomType);
    if(!empty($objectTypeManager)){
        $_SESSION["validation"]="Modification du type de prothese effectuée";
        header("location:?path=type&action=type");
    }
    else{
        $_SESSION["erreur"]="La modification du type de prothese n'a pas fonctionée";
        header("location:?path=type&action=type");
    }
    break;


case "traitementDeleteType":
    $id=filter_var($_POST["id"],FILTER_SANITIZE_SPECIAL_CHARS);
    $objectTypeManager=new TypeManager($lePDO);
    $objectTypeManager->deleteType($id);
    if($objectTypeManager==true){
        $_SESSION["validation"]="Supression du type de prothese effectuée";
        header("location:?path=type&action=type");
    }else{
        $_SESSION["erreur"]="La supression du type de prothese n'a pas fonctionée";
        header("location:?path=type&action=type");
    }
    break;


    default :
    require('view/404.php');
}