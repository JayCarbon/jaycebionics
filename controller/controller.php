<?php
$action=filter_var($_GET["action"]??"accueil",FILTER_SANITIZE_FULL_SPECIAL_CHARS);

switch ($action){
    case "accueil":
        require('view/accueil.php');
        break;
    case "prothesebras" :
        require ('view/protheseBras.php');
        break;
    case "prothesejambes" :
        require ('view/protheseJambes.php');
        break;
    case "achat" :
        require ('view/achat.php');
        break;
    case "formLogin":
        require("view/client/login.php");
        break;
        case "traitementLogin":
            $email=filter_var($_POST['email'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $mdp=filter_var($_POST['mdp'], FILTER_SANITIZE_FULL_SPECIAL_CHARS);
            $mdp=hash('SHA256', $mdp);
    
            $objectUtilisateurManager=new UtilisateurManager($lePDO);
            $utilisateur=$objectUtilisateurManager->fetchUtilisateurByEmailAndMdp($email, $mdp);
            if(empty($utilisateur)){
                $_SESSION['erreur']=[];
                array_push($_SESSION['erreur'], "La connexion a échoué");
                var_dump($utilisateur);
                header('location:?path=main&action=formLogin');
            }
            else{
                $_SESSION['idUtilisateur']=$utilisateur->getIdUtilisateur();
                $_SESSION['nom']=$utilisateur->getNom();
                $_SESSION['prenom']=$utilisateur->getPrenom();
                $_SESSION['email']=$utilisateur->getEmail();
                $_SESSION['idRole']=$utilisateur->getIdRole();
                header('location:./');
            }
            break;
    case "logout":
        session_unset();
        session_destroy();
        header("location:?path=main&action=formLogin");
        break;
    case "formInscription":
        require("view/client/inscription.php");
        break;
    case "traitementInscription" :
        $prenom=filter_var($_POST["prenom"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $nom=filter_var($_POST["nom"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $adresse=filter_var($_POST["adresse"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $ville=filter_var($_POST["ville"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $codepostal=filter_var($_POST["codepostal"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $email=filter_var($_POST["email"],FILTER_SANITIZE_EMAIL);
        $mdp=filter_var($_POST["mdp1"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $mdp2=filter_var($_POST["mdp2"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idRole=filter_var($_POST["role"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        if($mdp!=$mdp2){
            $_SESSION["error"]="Veuillez confirmer votre mot de passe.";
            header("location:./?path=main&action=formInscription");
            exit;
        }
        $hashedPassword=hash("SHA256",$mdp);
        $objectUtilisateurManager=new UtilisateurManager($lePDO); 
        $resultat=$objectUtilisateurManager->createUtilisateur($prenom,$nom,$email,$hashedPassword,$adresse,$ville,$codepostal,$idRole);
        if($resultat==true){
            $_SESSION["validation"]=["Validation de l'inscription."];
            header("location:?path=main&action=formLogin"); //rediriger vers la page de login
        }
        else{
            $_SESSION["erreur"]=["Échec de l'inscription."];
            header("location:?path=main&action=formInscription");
        }
        break; 
    case "apropos" :
        require ('view/aPropos.php');
        break;
    case "mensionslegales" :
        require ('view/mensionsLegales.php');
        break;
    case "contact" :
        require ('view/contact.php');
        break;
    break;
    default:
    require('view/404.php');
}
?>