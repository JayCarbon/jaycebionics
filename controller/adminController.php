<?php


$action=filter_var($_GET["action"]??"admin",FILTER_SANITIZE_FULL_SPECIAL_CHARS);

switch ($action){
    case "admin":
        require('view/admin/admin.php');
        break;
    case "adminProthese":
        $objetProtheseManager=new ProtheseManager($lePDO);
        $protheses=$objetProtheseManager->fetchAllProthese();
        $objetCouleurManager=new CouleurManager($lePDO);
        $objetOrientationManager=new OrientationManager($lePDO);
        $objetModeleManager=new ModeleManager($lePDO);
        require("view/prothese/adminProthese.php");
        break;
        
    case "createProthese":
        $objetTypeManager=new TypeManager($lePDO);
        $lesTypes=$objetTypeManager->fetchAllType();
        $objetCouleurManager=new CouleurManager($lePDO);
        $lesCouleurs=$objetCouleurManager->fetchAllCouleur();
        $objetOrientationManager=new OrientationManager($lePDO);
        $lesOrientations=$objetOrientationManager->fetchAllOrientation();
        $objetModeleManager=new ModeleManager($lePDO);
        $lesModeles=$objetModeleManager->fetchAllModele();
        require('view/prothese/createProthese.php');
        break;
    case "traitementCreateProthese":
        $image=filter_var($_POST['image'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $prix=filter_var($_POST['prix'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idModele=filter_var($_POST['idModele'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idCouleur=filter_var($_POST['idCouleur'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idOrientation=filter_var($_POST['idOrientation'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idType=filter_var($_POST['idType'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
     
        $objetProtheseManager=new ProtheseManager($lePDO);
             
        $idProthese=$objetProtheseManager->createProthese($image,$prix,$idModele,$idCouleur,$idOrientation,$idType);

        $_SESSION['msg']="Prothese Ajouter ".$nom;
        header("location:?path=admin&action=adminProthese");
     
        break;
    case "updateProthese":
        $id=filter_var($_GET["id"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $image=filter_var($_GET['image'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $prix=filter_var($_GET['prix'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idModele=filter_var($_GET['idModele'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idCouleur=filter_var($_GET['idCouleur'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idOrientation=filter_var($_GET['idOrientation'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idType=filter_var($_GET['idType'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $objetTypeManager=new TypeManager($lePDO);
        $lesTypes=$objetTypeManager->fetchAllType();
        $objetCouleurManager=new CouleurManager($lePDO);
        $lesCouleurs=$objetCouleurManager->fetchAllCouleur();
        $objetOrientationManager=new OrientationManager($lePDO);
        $lesOrientations=$objetOrientationManager->fetchAllOrientation();
        $objetModeleManager=new ModeleManager($lePDO);
        $lesModeles=$objetModeleManager->fetchAllModele();
        require('view/prothese/updateProthese.php');
        break;
    case "traitementUpdateProthese":
        $id=filter_var($_POST["id"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $image=filter_var($_POST['image'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $prix=filter_var($_POST['prix'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idModele=filter_var($_POST['idModele'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idCouleur=filter_var($_POST['idCouleur'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idOrientation=filter_var($_POST['idOrientation'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idType=filter_var($_POST['idType'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $objectProtheseManager=new ProtheseManager($lePDO);
        $objectProtheseManager->updateProthese($id,$image,$prix,$idModele,$idCouleur,$idOrientation,$idType);
        if($objectProtheseManager==true){
            //$_SESSION["validation"]="Modification de la prothese effectuée";
            header("location:?path=admin&action=adminProthese");
        }
        else{
            //$_SESSION["erreur"]="La modification de de la prothese n'a pas fonctionée";
            header("location:?path=admin&action=adminProthese");
        }
        require('view/prothese/updateProthese.php');
        break;
    case "traitementDeleteProthese":
        $id=filter_var($_POST["id"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $objectProtheseManager=new ProtheseManager($lePDO);
        $objectProtheseManager->deleteProthese($id);
        if($objectProtheseManager==true){
            //$_SESSION["validation"]="Supression de l'utilisateur effectuée";
            header("location:?path=admin&action=adminProthese");
        }else{
            //$_SESSION["erreur"]="La supression de l'utilisateur n'a pas fonctionée";
            header("location:?path=admin&action=adminProthese");
        }
        require('view/admin/adminProthese.php');
        break;

    case "gestionUtilisateur":
        $objectUtilisateurManager=new UtilisateurManager($lePDO);
        $utilisateurs=$objectUtilisateurManager->fetchAllUtilisateur();//utilisateurs dans le for each
        require('view/admin/gestionUtilisateurs.php');
        break;
    case "updateUtilisateur":
        $id=filter_var($_GET["id"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $nom=filter_var($_GET["nom"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $prenom=filter_var($_GET["prenom"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $email=filter_var($_GET["email"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $mdp=filter_var($_GET["mdp"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $adresse=filter_var($_GET["adresse"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $ville=filter_var($_GET["ville"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $codePostal=filter_var($_GET["codePostal"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idRole=filter_var($_GET["idRole"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        require('view/admin/updateUtilisateurs.php');
        break;
    case "updateUtilisateurTraitement":
        $id=filter_var($_POST["id"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $nom=filter_var($_POST["nom"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $prenom=filter_var($_POST["prenom"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $email=filter_var($_POST["email"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $mdp=filter_var($_POST["mdp"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $adresse=filter_var($_POST["adresse"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $ville=filter_var($_POST["ville"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $codePostal=filter_var($_POST["codePostal"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idRole=filter_var($_POST["idRole"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $hashedPassword=hash("SHA256",$mdp);
        $objectUtilisateurManager=new UtilisateurManager($lePDO);
        $objectUtilisateurManager->updateUtilisateur($id,$nom,$prenom,$email,$hashedPassword,$adresse,$ville,$codePostal,$idRole);
        if($objectTypeManager==true){
            //$_SESSION["validation"]="Modification de l'utilisateur effectuée";
            header("location:?path=admin&action=gestionUtilisateur");
        }
        else{
            //$_SESSION["erreur"]="La modification de l'utilisateur n'a pas fonctionée";
            header("location:?path=admin&action=gestionUtilisateur");
        }
        require('view/admin/updateUtilisateurs.php');
        break;
    case "deleteUtilisateurTraitement":
        $id=filter_var($_POST["id"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $objectUtilisateurManager=new UtilisateurManager($lePDO);
        $objectUtilisateurManager->deleteUtilisateur($id);
        if($objectUtilisateurManager==true){
            //$_SESSION["validation"]="Supression de l'utilisateur effectuée";
            header("location:?path=admin&action=gestionUtilisateur");
        }else{
            //$_SESSION["erreur"]="La supression de l'utilisateur n'a pas fonctionée";
            header("location:?path=admin&action=gestionUtilisateur");
        }
        require('view/admin/gestionUtilisateurs.php');
        break;
    default :
    require('view/404.php');

}

?>