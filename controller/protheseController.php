<?php
$action=filter_var($_GET["action"]??"article",FILTER_SANITIZE_FULL_SPECIAL_CHARS);

switch ($action){
    case "prothese":
        $objetTypeManager=new TypeManager($lePDO);
        $lesTypes=$objetTypeManager->fetchAllType();
        $objetCouleurManager=new CouleurManager($lePDO);
        $lesCouleurs=$objetCouleurManager->fetchAllCouleur();
        $objetOrientationManager=new OrientationManager($lePDO);
        $lesOrientations=$objetOrientationManager->fetchAllOrientation();
        $objetModeleManager=new ModeleManager($lePDO);
        $lesModeles=$objetModeleManager->fetchAllModele();
        require("view/prothese/viewProthese.php");
    break;
    case "TestProthese":
        $idModele=filter_var($_POST['idModele'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idCouleur=filter_var($_POST['idCouleur'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idOrientation=filter_var($_POST['idOrientation'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        $idType=filter_var($_POST['idType'],FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        $objectProtheseManager=new ProtheseManager($lePDO);
        $prothese=$objectProtheseManager->fetchIdProtheseByValues($idModele,$idCouleur,$idOrientation,$idType);

        $objectModeleManager= new ModeleManager($lePDO);
        $nomModele=$objectModeleManager->fetchModeleById($prothese->getIdModele())->getNom();

        $objectCouleurManager= new CouleurManager($lePDO);
        $nomCouleur=$objectCouleurManager->fetchCouleurById($prothese->getIdCouleur())->getCouleur();

        $objectOrientationManager= new OrientationManager($lePDO);
        $nomOrientation=$objectOrientationManager->fetchOrientationById($prothese->getIdOrientation())->getOrientation();

        if(isset($_SESSION["panier"]))
        {
            $_SESSION['panier'][$prothese->getIdProthese()]=['id'=>$prothese->getIdProthese(),"nom"=>$nomModele,"couleur"=>$nomCouleur,"orientation"=>$nomOrientation,"prix"=>$prothese->getPrix(),"image"=>$prothese->getImage()];
        }
        else{
            $_SESSION['panier']=[];
            $_SESSION['panier'][$prothese->getIdProthese()]=['id'=>$prothese->getIdProthese(),"nom"=>$nomModele,"couleur"=>$nomCouleur,"orientation"=>$nomOrientation,"prix"=>$prothese->getPrix(),"image"=>$prothese->getImage()];
        }
        if($prothese==true){
            header("location:?path=article&action=panier");
        }
        else{
            header("location:?path=article&action=panier");
        }
        break;
        require("view/prothese/viewProthese.php");
    break;
    case "panier":
        // $_SESSION["panier"]=[];
        // var_dump($_SESSION['panier']);
        $objetProtheseManager=new ProtheseManager($lePDO);
        require("view/client/panier.php");
        break;

    case "traitementDeleteProthesePanier":
        $id=filter_var($_POST["id"],FILTER_SANITIZE_FULL_SPECIAL_CHARS);
        if(isset($_SESSION["panier"][$id]))
        {
            unset($_SESSION['panier'][$id]);
        }
        // $objectProtheseManager=new ProtheseManager($lePDO);
        // $objectProtheseManager->deleteProthese($id);
        header("location:?path=article&action=panier");
        // if($objectProtheseManager==true){
           
        // }else{
        //     header("location:?path=client&action=panier");
        // }
        require('view/admin/adminProthese.php');
        break;

    case "valideCommande":

        require("view/client/commandes.php");
        break;      
    default :
    require('view/404.php');
}