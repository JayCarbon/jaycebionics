<?php
session_start();
require("model/bdd.php");
//Chargement automatique des classes (classes métiers et managers)
spl_autoload_register(function ($class_name) {
    if(strstr($class_name,"Manager"))
    {
        include "model/manager/".$class_name.".php";
    }
    else{
        include "model/class/".$class_name . '.class.php';
    }
    
});

//La variable $lePDO établie la connexion à la base de donnée
$lePDO=connexionBDD();
$path=filter_var($_GET['path']??"main",FILTER_SANITIZE_FULL_SPECIAL_CHARS);

switch ($path){
    case "main":
        require('controller/controller.php');
        break;

    case "type":
        if (empty($_SESSION['idRole']))
        {
        require("view/404.php");
        }
        elseif($_SESSION['idRole']=="2")
        {
        require("controller/typeController.php");
        }
        else{
        require("view/404.php");
        }
        break;

    case "article":
        require("controller/protheseController.php");
        break;

    case "client":
            if (empty($_SESSION['idRole']))
            {
            require("view/404.php");
            }
            elseif($_SESSION['idRole']=="1")
            {
            require('controller/clientController.php');;
            }
            else{
            require("view/404.php");
            }
            break;

        case "admin":
            if (empty($_SESSION['idRole']))
            {
            require("view/404.php");
            }
            elseif($_SESSION['idRole']=="2")
            {
            require('controller/adminController.php');
            }
            else{
            require("view/404.php");
            }
    break;
    default:
    require("view/404.php");
}
?>