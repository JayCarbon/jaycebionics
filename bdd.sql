-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : lun. 23 mai 2022 à 15:45
-- Version du serveur : 5.7.36
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `jaycebionics`
--

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `idCommande` int(11) NOT NULL AUTO_INCREMENT,
  `dateCommande` date NOT NULL,
  `statutCommande` varchar(50) DEFAULT NULL,
  `dateLivraison` datetime NOT NULL,
  `idUtilisateur` int(11) NOT NULL,
  PRIMARY KEY (`idCommande`),
  KEY `idUtilisateur` (`idUtilisateur`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `commande_prothese`
--

DROP TABLE IF EXISTS `commande_prothese`;
CREATE TABLE IF NOT EXISTS `commande_prothese` (
  `idCommande` int(11) NOT NULL,
  `idProthese` int(11) NOT NULL,
  PRIMARY KEY (`idCommande`,`idProthese`),
  KEY `idProthese` (`idProthese`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `couleur`
--

DROP TABLE IF EXISTS `couleur`;
CREATE TABLE IF NOT EXISTS `couleur` (
  `idCouleur` int(11) NOT NULL AUTO_INCREMENT,
  `couleur` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idCouleur`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `couleur`
--

INSERT INTO `couleur` (`idCouleur`, `couleur`) VALUES
(1, 'Rouge'),
(2, 'Vert'),
(3, 'Bleu'),
(4, 'Jaune'),
(5, 'Gris'),
(6, 'Blanc');

-- --------------------------------------------------------

--
-- Structure de la table `modele`
--

DROP TABLE IF EXISTS `modele`;
CREATE TABLE IF NOT EXISTS `modele` (
  `idModele` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idModele`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `modele`
--

INSERT INTO `modele` (`idModele`, `nom`) VALUES
(1, 'HeroArm'),
(2, 'Circleg');

-- --------------------------------------------------------

--
-- Structure de la table `orientation`
--

DROP TABLE IF EXISTS `orientation`;
CREATE TABLE IF NOT EXISTS `orientation` (
  `idOrientation` int(11) NOT NULL AUTO_INCREMENT,
  `orientation` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idOrientation`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `orientation`
--

INSERT INTO `orientation` (`idOrientation`, `orientation`) VALUES
(1, 'Gauche'),
(2, 'Droite');

-- --------------------------------------------------------

--
-- Structure de la table `prothese`
--

DROP TABLE IF EXISTS `prothese`;
CREATE TABLE IF NOT EXISTS `prothese` (
  `idProthese` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(50) DEFAULT NULL,
  `prix` int(4) DEFAULT NULL,
  `idModele` int(11) NOT NULL,
  `idCouleur` int(11) NOT NULL,
  `idOrientation` int(11) NOT NULL,
  `idType` int(11) NOT NULL,
  PRIMARY KEY (`idProthese`),
  KEY `idModele` (`idModele`),
  KEY `idCouleur` (`idCouleur`),
  KEY `idOrientation` (`idOrientation`),
  KEY `idType` (`idType`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `prothese`
--

INSERT INTO `prothese` (`idProthese`, `image`, `prix`, `idModele`, `idCouleur`, `idOrientation`, `idType`) VALUES
(1, '1_1_1_1.jpeg', 899, 1, 1, 1, 1),
(2, '1_1_1_2.jpeg', 899, 1, 1, 2, 1),
(3, '1_1_2_1.jpeg', 899, 1, 2, 1, 1),
(4, '1_1_2_2.jpeg', 899, 1, 2, 2, 1),
(5, '1_1_3_1.jpeg', 899, 1, 3, 1, 1),
(6, '1_1_3_2.jpeg', 899, 1, 3, 2, 1),
(7, '1_1_4_1.jpeg', 899, 1, 4, 1, 1),
(8, '1_1_4_2.jpeg', 899, 1, 4, 2, 1),
(9, '1_1_5_1.jpeg', 899, 1, 5, 1, 1),
(10, '1_1_5_2.jpeg', 899, 1, 5, 2, 1),
(11, '1_1_6_1.jpeg', 899, 1, 6, 1, 1),
(12, '1_1_6_2.jpeg', 899, 1, 6, 2, 1),
(13, '2_2_1_1.jpeg', 899, 2, 1, 1, 2),
(14, '2_2_1_2.jpeg', 899, 2, 1, 2, 2),
(15, '2_2_2_1.jpeg', 899, 2, 2, 1, 2),
(16, '2_2_2_2.jpeg', 899, 2, 2, 2, 2),
(17, '2_2_3_1.jpeg', 899, 2, 3, 1, 2),
(18, '2_2_3_2.jpeg', 899, 2, 3, 2, 2),
(19, '2_2_4_1.jpeg', 899, 2, 4, 1, 2),
(20, '2_2_4_2.jpeg', 899, 2, 4, 2, 2),
(21, '2_2_5_1.jpeg', 899, 2, 5, 1, 2),
(22, '2_2_5_2.jpeg', 899, 2, 5, 2, 2),
(23, '2_2_6_1.jpeg', 899, 2, 6, 1, 2),
(24, '2_2_6_2.jpeg', 899, 2, 6, 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `idRole` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  PRIMARY KEY (`idRole`),
  UNIQUE KEY `nom` (`nom`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`idRole`, `nom`) VALUES
(1, 'client'),
(2, 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `type`
--

DROP TABLE IF EXISTS `type`;
CREATE TABLE IF NOT EXISTS `type` (
  `idType` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idType`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `type`
--

INSERT INTO `type` (`idType`, `nom`) VALUES
(1, 'Protheses pour Bras'),
(2, 'Protheses pour Jambes');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `mdp` varchar(500) NOT NULL,
  `adresse` varchar(100) DEFAULT NULL,
  `ville` varchar(100) DEFAULT NULL,
  `codePostal` int(11) DEFAULT NULL,
  `idRole` int(11) NOT NULL,
  PRIMARY KEY (`idUtilisateur`),
  UNIQUE KEY `email` (`email`),
  KEY `idRole` (`idRole`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`idUtilisateur`, `nom`, `prenom`, `email`, `mdp`, `adresse`, `ville`, `codePostal`, `idRole`) VALUES
(1, 'Jaison', 'Carbon', 'carbonjaison@gmail.com', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', 'Adresse', 'Ville', 75000, 2);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
