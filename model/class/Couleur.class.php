<?php
class Couleur {
    private $idCouleur;
    private $couleur;

    /**
     * Get the value of idCouleur
     */ 
    public function getIdCouleur()
    {
        return $this->idCouleur;
    }

    /**
     * Set the value of idCouleur
     *
     * @return  self
     */ 
    public function setIdCouleur($idCouleur)
    {
        $this->idCouleur = $idCouleur;

        return $this;
    }

    /**
     * Get the value of couleur
     */ 
    public function getCouleur()
    {
        return $this->couleur;
    }

    /**
     * Set the value of couleur
     *
     * @return  self
     */ 
    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;

        return $this;
    }
}

?>