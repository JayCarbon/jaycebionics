<?php
class Prothese {
    private $idProthese;
    private $image;
    private $prix;
    private $idModele;
    private $idCouleur;
    private $idOrientation;
    private $idType;


    /**
     * Get the value of idProthese
     */ 
    public function getIdProthese()
    {
        return $this->idProthese;
    }

    /**
     * Set the value of idProthese
     *
     * @return  self
     */ 
    public function setIdProthese($idProthese)
    {
        $this->idProthese = $idProthese;

        return $this;
    }

    /**
     * Get the value of image
     */ 
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set the value of image
     *
     * @return  self
     */ 
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get the value of prix
     */ 
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set the value of prix
     *
     * @return  self
     */ 
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get the value of idModele
     */ 
    public function getIdModele()
    {
        return $this->idModele;
    }

    /**
     * Set the value of idModele
     *
     * @return  self
     */ 
    public function setIdModele($idModele)
    {
        $this->idModele = $idModele;

        return $this;
    }

    /**
     * Get the value of idCouleur
     */ 
    public function getIdCouleur()
    {
        return $this->idCouleur;
    }

    /**
     * Set the value of idCouleur
     *
     * @return  self
     */ 
    public function setIdCouleur($idCouleur)
    {
        $this->idCouleur = $idCouleur;

        return $this;
    }

    /**
     * Get the value of idOrientation
     */ 
    public function getIdOrientation()
    {
        return $this->idOrientation;
    }

    /**
     * Set the value of idOrientation
     *
     * @return  self
     */ 
    public function setIdOrientation($idOrientation)
    {
        $this->idOrientation = $idOrientation;

        return $this;
    }

    /**
     * Get the value of idType
     */ 
    public function getIdType()
    {
        return $this->idType;
    }

    /**
     * Set the value of idType
     *
     * @return  self
     */ 
    public function setIdType($idType)
    {
        $this->idType = $idType;

        return $this;
    }
}

?>