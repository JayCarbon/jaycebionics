<?php
class Commande {
    private $idCommande;
    private $nomCommande;
    private $dateCommande;
    private $statutCommande;
    private $dateLivraison;
    private $idUtilisateur;

    /**
     * Get the value of idCommande
     */ 
    public function getIdCommande()
    {
        return $this->idCommande;
    }

    /**
     * Set the value of idCommande
     *
     * @return  self
     */ 
    public function setIdCommande($idCommande)
    {
        $this->idCommande = $idCommande;

        return $this;
    }

    /**
     * Get the value of dateCommande
     */ 
    public function getDateCommande()
    {
        return $this->dateCommande;
    }

    /**
     * Set the value of dateCommande
     *
     * @return  self
     */ 
    public function setDateCommande($dateCommande)
    {
        $this->dateCommande = $dateCommande;

        return $this;
    }

    /**
     * Get the value of statutCommande
     */ 
    public function getStatutCommande()
    {
        return $this->statutCommande;
    }

    /**
     * Set the value of statutCommande
     *
     * @return  self
     */ 
    public function setStatutCommande($statutCommande)
    {
        $this->statutCommande = $statutCommande;

        return $this;
    }

    /**
     * Get the value of dateLivraison
     */ 
    public function getDateLivraison()
    {
        return $this->dateLivraison;
    }

    /**
     * Set the value of dateLivraison
     *
     * @return  self
     */ 
    public function setDateLivraison($dateLivraison)
    {
        $this->dateLivraison = $dateLivraison;

        return $this;
    }

    /**
     * Get the value of idUtilisateur
     */ 
    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }

    /**
     * Set the value of idUtilisateur
     *
     * @return  self
     */ 
    public function setIdUtilisateur($idUtilisateur)
    {
        $this->idUtilisateur = $idUtilisateur;

        return $this;
    }

    /**
     * Get the value of nomCommande
     */ 
    public function getNomCommande()
    {
        return $this->nomCommande;
    }

    /**
     * Set the value of nomCommande
     *
     * @return  self
     */ 
    public function setNomCommande($nomCommande)
    {
        $this->nomCommande = $nomCommande;

        return $this;
    }
}

?>