<?php
class Modele {
    private $idModele;
    private $nom;


    /**
     * Get the value of idModele
     */ 
    public function getIdModele()
    {
        return $this->idModele;
    }

    /**
     * Set the value of idModele
     *
     * @return  self
     */ 
    public function setIdModele($idModele)
    {
        $this->idModele = $idModele;

        return $this;
    }

    /**
     * Get the value of nom
     */ 
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set the value of nom
     *
     * @return  self
     */ 
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }
}

?>