<?php
class Orientation {
    private $idOrientation;
    private $orientation;

    /**
     * Get the value of idOrientation
     */ 
    public function getIdOrientation()
    {
        return $this->idOrientation;
    }

    /**
     * Set the value of idOrientation
     *
     * @return  self
     */ 
    public function setIdOrientation($idOrientation)
    {
        $this->idOrientation = $idOrientation;

        return $this;
    }

    /**
     * Get the value of orientation
     */ 
    public function getOrientation()
    {
        return $this->orientation;
    }

    /**
     * Set the value of orientation
     *
     * @return  self
     */ 
    public function setOrientation($orientation)
    {
        $this->orientation = $orientation;

        return $this;
    }
}

?>