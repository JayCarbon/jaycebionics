<?php
class CouleurManager{
private $lePDO;

public function __construct($unPDO)
{
    $this->lePDO=$unPDO;
}

/**
 * Fonction qui permet de recuperer toutes les couleurs
 *
 * @return array Les couleurs dans un array a 2 dimensions
 */
function fetchAllCouleur(){

    try {
        //Pour la co on utilise l'attribut lePDO
        $connex=$this->lePDO;
        $sql =$connex->prepare("SELECT * FROM couleur ORDER BY idCouleur");
        $sql->execute();
        $sql->setFetchMode(PDO::FETCH_CLASS,"Couleur");
        $resultat = ($sql->fetchAll());
        return $resultat;

    } catch (PDOException $error) {
        echo $error->getMessage();
    }
}

 /**
     * Permet de recuperer une type par son id
     *
     * @param [type] $id l'id de la couleur
     * @return object la couleur sous la forme d'un array
     */
    function fetchCouleurById($id)
    {
        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM couleur WHERE idCouleur=:idCouleur");
            $sql->bindParam(":idCouleur",$id);
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Couleur");
            $resultat = ($sql->fetch());
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

}
?>