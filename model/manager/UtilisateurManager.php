<?php
class UtilisateurManager{

    private $lePDO;

    public function __construct($unPDO)
    {
        $this->lePDO=$unPDO;
    }

    function fetchAllUtilisateur(){

        try {
            //Pour la co on utilise l'attribut lePDO
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM utilisateur ORDER BY idUtilisateur");
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Utilisateur");
            $resultat = ($sql->fetchAll());
            return $resultat;
    
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    public function fetchUtilisateurByEmailAndMdp($email, $hashMdp)
    {
        try{

       
        $connex=$this->lePDO;
        $sql=$connex->prepare("SELECT * FROM utilisateur where email=:email and mdp=:mdp");
        
        $sql->bindParam(":email",$email);
        $sql->bindParam(":mdp",$hashMdp);
        $sql->execute();

        $sql->setFetchMode(PDO::FETCH_CLASS,"Utilisateur");
        $resultat=$sql->fetch();
        return $resultat;
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
    }

    public function createUtilisateur($prenom,$nom,$email,$mdp,$adresse,$ville,$codePostal,$idRole){
        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("INSERT INTO utilisateur values(null,:prenom,:nom,:email,:mdp,:adresse,:ville,:codePostal,:idRole)");
            $sql->bindParam(":prenom",$prenom);
            $sql->bindParam(":nom",$nom);
            $sql->bindParam(":email",$email);
            $sql->bindParam(":mdp",$mdp);
            $sql->bindParam(":adresse",$adresse);
            $sql->bindParam(":ville",$ville);
            $sql->bindParam(":codePostal",$codePostal);
            $sql->bindParam(":idRole",$idRole);
            $sql->execute();
            return true;
    
    
        } catch (PDOException $error) {
            echo $error->getMessage();
        } 
    }

    public function updateUtilisateur($unId,$unNom,$unPrenom,$unEmail,$unMdp,$uneAdresse,$uneVille,$unCodePostal,$unIdRole){
        try{
            $connex=$this->lePDO;
            $sql=$connex->prepare("UPDATE utilisateur SET nom=:nom, prenom=:prenom, email=:email, mdp=:mdp, adresse=:adresse, ville=:ville, codePostal=:codePostal, idRole=:idRole WHERE idUtilisateur=:idUtilisateur");
            $sql->bindParam(":nom",$unNom);
            $sql->bindParam(":prenom",$unPrenom);
            $sql->bindParam(":email",$unEmail);
            $sql->bindParam(":mdp",$unMdp);
            $sql->bindParam(":adresse",$uneAdresse);
            $sql->bindParam(":ville",$uneVille);
            $sql->bindParam(":codePostal",$unCodePostal);
            $sql->bindParam(":idRole",$unIdRole);
            $sql->bindParam(":idUtilisateur",$unId);
            $sql->execute();
            return true;
        }catch(PDOException $error){
            echo $error->getMessage();
            return false;
        }
    }

    public function deleteUtilisateur($unId){
        try{
            $connex=$this->lePDO;
            $sql=$connex->prepare("DELETE FROM utilisateur WHERE idUtilisateur=:idUtilisateur");
            $sql->bindParam(":idUtilisateur",$unId);
            $sql->execute();
            return true;
        }
        catch (PDOException $error){
            echo $error->getMessage();
            return false;
        }
    }

}
?>