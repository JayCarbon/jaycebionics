<?php
class OrientationManager{
private $lePDO;

public function __construct($unPDO)
{
    $this->lePDO=$unPDO;
}

/**
 * Fonction qui permet de recuperer toutes les orientations
 *
 * @return array Les orientations dans un array a 2 dimensions
 */
function fetchAllOrientation(){

    try {
        //Pour la co on utilise l'attribut lePDO
        $connex=$this->lePDO;
        $sql =$connex->prepare("SELECT * FROM orientation ORDER BY idOrientation");
        $sql->execute();
        $sql->setFetchMode(PDO::FETCH_CLASS,"Orientation");
        $resultat = ($sql->fetchAll());
        return $resultat;

    } catch (PDOException $error) {
        echo $error->getMessage();
    }
}

 /**
     * Permet de recuperer une type par son id
     *
     * @param [type] $id l'id de l'orientation
     * @return object l'orientation sous la forme d'un array
     */
    function fetchOrientationById($id)
    {
        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM orientation WHERE idOrientation=:idOrientation");
            $sql->bindParam(":idOrientation",$id);
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Orientation");
            $resultat = ($sql->fetch());
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

}
?>