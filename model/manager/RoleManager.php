<?php
class RoleManager{

    private $lePDO;

    public function __construct($unPDO)
    {
        $this->lePDO=$unPDO;
    }
    //Function qui permet de chercher tout les roles

    public function fetchAllRoles()
    {
        try{
        $connex=$this->lePDO;
        $sql=$connex->prepare("SELECT * FROM role");

        $sql->execute();

        $sql->setFetchMode(PDO::FETCH_CLASS,"Role");
        $resultat=$sql->fetchAll();
        return $resultat;
    }
    catch(PDOException $e)
    {
        echo $e->getMessage();
    }
    }
}
?>