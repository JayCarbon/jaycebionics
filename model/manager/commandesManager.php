<?php

class CommandeManager{
private $lePDO;

public function __construct($unPDO)
{
    $this->lePDO=$unPDO;
}


function createCommande(){
    try {
        //idCommande 	dateCommande 	dateLivraison 	etat 	idClient 
        $connex=$this->lePDO;
        $connex->beginTransaction();
        $sql =$connex->prepare("INSERT INTO commande values(null,:dateCommande,'En cours',null,:idUtilisateur)");
        $today = date("Y-m-d H:i:s");   
        $sql->bindParam(":dateCommande",$today);
        $sql->bindValue(":idUtilisateur",$_SESSION['id']);
        $sql->execute();

        //idArticle 	idCommande 	quantiteArticle 
        $idCommande=$connex->lastInsertId();
        foreach($_SESSION['panier'] as $uneLignePanier)
        {
        $sql =$connex->prepare("INSERT INTO article_commande values(:idArticle,:idCommande,:quantite)");
        
        $sql->bindParam(":idCommande",$idCommande);
        $sql->bindValue(":idArticle",$uneLignePanier[0]);
        $sql->bindValue(":quantite",$uneLignePanier[1]);
        $sql->execute();
        }
        $connex->commit();
        return true;

    } catch (PDOException $error) {
        $connex->rollBack();
        echo $error->getMessage();
        return false;
    }
}
function fetchCommandeById($idClient){
    try{
        $connex=$this->lePDO;
        $sql=$connex->prepare("SELECT * FROM commande WHERE idClient = :idClient");
        $sql->bindParam(":idClient",$idClient);
        $sql->execute();
        $sql->setFetchMode(PDO::FETCH_CLASS,"Commande");
        $resultat=$sql->fetchAll();
        return $resultat;
    }catch(PDOException $e)
    {
        echo $e->getMessage();
    }
}
}
?>