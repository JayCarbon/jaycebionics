<?php

class ProtheseManager{
private $lePDO;

public function __construct($unPDO)
{
    $this->lePDO=$unPDO;
}



    /**
     * Permet de recuperer tout les prothese d'un type (par son id)
     *
     * @param [type] $idType
     * @return array un array a deux dimensions
     */
    function fetchAllProtheseByIdType($idType){

        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM prothese WHERE idType=:idType ORDER BY nom");
            $sql->bindParam(":idType",$idType);
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Prothese");
            $resultat = ($sql->fetchAll());
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    /**
     * Sauvegarde un nouvelle article dans la table article
     *
     * @param [type] $nom
     * @param [type] $couleur
     * @param [type] $image
     * @param [type] $prix
     * @param [type] $idType
     * @return int idProthese
     */
    function createProthese($image,$prix,$idCouleur,$idModele,$idOrientation,$idType){
        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("INSERT INTO prothese values(null,:image,:prix,:idModele,:idCouleur,:idOrientation,:idType)");
            $sql->bindParam(":image",$image);
            $sql->bindParam(":prix",$prix);
            $sql->bindParam(":idModele",$idModele);
            $sql->bindParam(":idCouleur",$idCouleur);
            $sql->bindParam(":idOrientation",$idOrientation);
            $sql->bindParam(":idType",$idType);
            $sql->execute();
            $idProthese=$connex->lastInsertId();
            return $idProthese;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    public function fetchProtheseByIdProthese($idProthese)
    {
        try{


        $connex=$this->lePDO;
        $sql=$connex->prepare("SELECT * FROM prothese WHERE idProthese=:idProthese");
        $sql->bindParam(":idProthese",$idProthese);
        $sql->execute();
        //Definir le mode de récuperation des donnees
        // par défaut on recup les données sous forme array 
        // si on veut recupere nos donner sous la forme d'objet d'une de nos classes
        //on utiliser setFetchMode
        $sql->setFetchMode(PDO::FETCH_CLASS,"Prothese");
        $resultat=$sql->fetch();
        return $resultat;
    }
    catch(PDOException $e)
    {
        echo "Fichier : ".$e->getFile()."<br>";
        echo "Ligne : ".$e->getLine()."<br>";
        echo $e->getMessage();
    }

    }

    function fetchAllProthese(){

        try {
            //Pour la co on utilise l'attribut lePDO
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM prothese ORDER BY idProthese");
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Prothese");
            $resultat = ($sql->fetchAll());
            return $resultat;
    
        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    public function updateProthese($unId,$uneImage,$unPrix,$unidModele,$unidCouleur,$unidOrientation,$unIdType){
        try{
            $connex=$this->lePDO;
            $sql=$connex->prepare("UPDATE prothese SET image=:image, prix=:prix, idModele=:idModele, idCouleur=:idCouleur, idOrientation=:idOrientation, idType=:idType WHERE idProthese=:idProthese");
            $sql->bindParam(":image",$uneImage);
            $sql->bindParam(":prix",$unPrix);
            $sql->bindParam(":idModele",$unidModele);
            $sql->bindParam(":idCouleur",$unidCouleur);
            $sql->bindParam(":idOrientation",$unidOrientation);
            $sql->bindParam(":idType",$unIdType);
            $sql->bindParam(":idProthese",$unId);
            $sql->execute();
            return true;
        }catch(PDOException $error){
            echo $error->getMessage();
            return false;
        }
    }

    
    public function deleteProthese($unId){
        try{
            $connex=$this->lePDO;
            $sql=$connex->prepare("DELETE FROM prothese WHERE idProthese=:idProthese");
            $sql->bindParam(":idProthese",$unId);
            $sql->execute();
            return true;
        }
        catch (PDOException $error){
            echo $error->getMessage();
            return false;
        }
    }

    function fetchIdProtheseByValues($idModele,$idCouleur,$idOrientation,$idType){

        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM prothese  WHERE idModele=:idModele AND idCouleur=:idCouleur AND idOrientation=:idOrientation AND idType=:idType");
            $sql->bindParam(":idModele",$idModele);
            $sql->bindParam(":idCouleur",$idCouleur);
            $sql->bindParam(":idOrientation",$idOrientation);
            $sql->bindParam(":idType",$idType);
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Prothese");
            $resultat = ($sql->fetch());
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }
}
?>