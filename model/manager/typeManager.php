<?php
class TypeManager{
private $lePDO;

public function __construct($unPDO)
{
    $this->lePDO=$unPDO;
}

/**
 * Fonction qui permet de recuperer toutes les types
 *
 * @return array Les types dans un array a 2 dimensions
 */
function fetchAllType(){

    try {
        //Pour la co on utilise l'attribut lePDO
        $connex=$this->lePDO;
        $sql =$connex->prepare("SELECT * FROM type ORDER BY idType");
        $sql->execute();
        $sql->setFetchMode(PDO::FETCH_CLASS,"Type");
        $resultat = ($sql->fetchAll());
        return $resultat;

    } catch (PDOException $error) {
        echo $error->getMessage();
    }
}

 /**
     * Permet de recuperer une type par son id
     *
     * @param [type] $id l'id de la type
     * @return array la type sous la forme d'un array
     */
    function fetchTypeById($id)
    {
        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM type WHERE idType=:idType");
            $sql->bindParam(":idType",$id);
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Type");
            $resultat = ($sql->fetch());
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

    public function createType($nomType){
        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("INSERT INTO type values(null,:nom)");
            $sql->bindParam(":nom",$nomType);
            $sql->execute();
            return true;
        } catch (PDOException $error) {
            echo $error->getMessage();
            return false;
        } 
    }
    public function updateType($unId,$unNom){
        try{
            $connex=$this->lePDO;
            $sql=$connex->prepare("UPDATE type SET nom=:nom WHERE idType=:idType");
            $sql->bindParam(":nom",$unNom);
            $sql->bindParam(":idType",$unId);
            $sql->execute();
            return true;
        }catch(PDOException $error){
            echo $error->getMessage();
            return false;
        }
    }

    public function deleteType($unId){
        try{
            $connex=$this->lePDO;
            $sql=$connex->prepare("DELETE FROM type WHERE idType=:idType");
            $sql->bindParam(":idType",$unId);
            $sql->execute();
            $sql2=$connex->prepare("DELETE FROM prothese WHERE idType=:idType");
            $sql2->bindParam(":idType",$unId);
            $sql2->execute();
            return true;
        }
        catch (PDOException $error){
            echo $error->getMessage();
            return false;
        }
    }

}
?>