<?php
class ModeleManager{
private $lePDO;

public function __construct($unPDO)
{
    $this->lePDO=$unPDO;
}

/**
 * Fonction qui permet de recuperer tout les modeles
 *
 * @return array Les modeles dans un array a 2 dimensions
 */
function fetchAllModele(){

    try {
        //Pour la co on utilise l'attribut lePDO
        $connex=$this->lePDO;
        $sql =$connex->prepare("SELECT * FROM modele ORDER BY idModele");
        $sql->execute();
        $sql->setFetchMode(PDO::FETCH_CLASS,"Modele");
        $resultat = ($sql->fetchAll());
        return $resultat;

    } catch (PDOException $error) {
        echo $error->getMessage();
    }
}

 /**
     * Permet de recuperer une type par son id
     *
     * @param [type] $id l'id de la modele
     * @return object la modele sous la forme d'un array
     */
    //mettre en object pas en array
    function fetchModeleById($id)
    {
        try {
            $connex=$this->lePDO;
            $sql =$connex->prepare("SELECT * FROM modele WHERE idModele=:idModele");
            $sql->bindParam(":idModele",$id);
            $sql->execute();
            $sql->setFetchMode(PDO::FETCH_CLASS,"Modele");
            $resultat = ($sql->fetch());
            return $resultat;

        } catch (PDOException $error) {
            echo $error->getMessage();
        }
    }

}
?>