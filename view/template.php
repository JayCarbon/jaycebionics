<!DOCTYPE html>

<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Vente de prothèses mécaniques">
    <meta name="keywords" content="Open Bionics, Hero Arm, Circleg">
    <!-- CSS bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <!-- CSS -->
    <link rel="stylesheet" href="asset/css/style.css">
    <!-- Inclus le titre des différentes pages-->
    <title><?php echo $title; ?></title>
</head>

<body class="d-flex flex-column min-vh-100 bg-dark">
    <!-- Inclus le menu -->
    <?php include "fragments/menu.php";?>
    <main>
    <!-- Inclus le contenu enregistré de ob_start et ob_get_clean par différentes pages-->
    <?php echo $content ; ?>
    </main>
    <!-- Inclus le footer -->
    <?php include "fragments/footer.php"; ?>


    <!-- JS bootstrap -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous">
    </script>
     <script src="asset/js/scripts.js"></script> 
</body>

</html>