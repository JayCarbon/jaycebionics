<?php
$title="Contact";
ob_start();?>

<!-- Main -->

<!-- Phrase 1 -->
<div class="container overflow-hidden">
    <div class="row gy-4">
        <div class="col-sm-12">
            <h4 class="py-3 text-center text-light">
                CONTACTEZ NOUS
            </h4>
            <h1 class="py-5 text-center text-light">COMMENT NOUS JOINDRE</h1>
        </div>
    </div>
</div>

<!-- Formulaire de contact -->
<div class="container overflow-hidden">
    <div class="row gy-4">
        <div class="col-sm-12 col-md-6">
            <img class="bionichand bg-light p-2 rounded" src="asset/images/robotic-hand.png" alt=""
                style="width: 300px; height: 400px;">
        </div>
        <div class="col-sm-12 col-md-6 text-light">
            <form>
                <div class="form-group">
                    <label for="exampleInputEmail1">Nom</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                        placeholder="Entrez votre nom">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Prenom</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                        placeholder="Entrez votre prenom">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Adresse e-amil</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                        placeholder="Entrez votre email">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Message</label>
                    <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"
                        placeholder="Entrez votre message"></textarea>
                </div>
                <button type="submit" class="btn btn-primary my-4">Valider</button>
            </form>
        </div>
    </div>
</div>

<div class="container overflow-hidden">
    <div class="row gy-8 m-5 align-items-center justify-content-evenly">
        <div class="col-sm-12 col-md-3">
            <p class="text-center text-light bg-info rounded">
                ADRESSE
                <br>
                61 Rue des Bordes, 94430 Chennevières-sur-Marne
            </p>
        </div>
        <div class="col-sm-12 col-md-3">
            <p class="text-center text-light bg-info rounded">
                E-MAIL
                <br><br>
                gmte94.champlain@ac-creteil.fr
            </p>
        </div>
        <div class="col-sm-12 col-md-3">
            <p class="text-center text-light bg-info rounded">
                TELEPHONE
                <br><br>
                01 56 86 19 10
            </p>
        </div>
    </div>
</div>

<!-- Image Maps -->
<div class="container overflow-hidden">
    <div class="row gy-4 align-items-center mx-auto">
        <div class="col-12">
            <img class="img-fluid rounded" src="asset/images/img/maps.png" alt="Responsive image">
        </div>
    </div>
</div>
<?php $content=ob_get_clean();
require("template.php");