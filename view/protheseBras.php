<?php
$title="Prothèses pour bras";
ob_start();?>

<!-- Main -->

<!-- Images du produits -->
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <a href="./?path=article&action=prothese"><img class="img-fluid rounded" src="asset/images/HeaderHeroArm.jpeg" alt="Responsive image"></a>
        </div>
    </div>
</div>


<!-- Phrase 1 -->
<div class="container overflow-hidden">
    <div class="row gy-4">
        <div class="col-sm-12">
            <div class="p-5 text-center text-light">
                <h1>Une prothèse conçue pour le fonctionnement et le confort</h1>
                <p class="p-5">Conçue spécialement pour vous, la Hero Arm est une prothèse myoélectrique légère et
                    abordable pour les adultes amputés sous le coude et les enfants de huit ans et plus. À l’aide de
                    l’Open Bionics Hero Arm, vous pouvez saisir, pincer, faire un high-five, coup de poing, pouce en
                    l’air. Bienvenue dans le futur, où les handicaps sont des superpouvoirs.
                </p>
            </div>
        </div>
    </div>
</div>


<!-- Images clients juniors -->
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <img class="img-fluid rounded" src="asset/images/CientsJuniorsOpenBionics.png" alt="Responsive image">
        </div>
    </div>
</div>


<!-- Image sur Mesure et Phrase 2 -->
<div class="container overflow-hidden">
    <div class="row gy-4">
        <div class="col-sm-12 col-md-6">
            <img class="img-fluid rounded" src="asset/images/SurMesureOpenBionics.jpg" alt="">
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="p-5 text-center text-light">
                <h4>Bionique super cool pour adultes et enfants</h4>
                <p class="p-4">On estime à cinq millions le nombre d’amputés des membres supérieurs dans le monde. Open
                    Bionics construit et développe la prochaine génération de membres bioniques et transforme les
                    handicaps en superpuissances. Chaque bras de héros est construit sur mesure et fonctionne en
                    ramassant des signaux des muscles d’un utilisateur. Nous avons construit un bras bionique pour un
                    confort et une fonctionnalité optimaux et maintenant avec plus de 50 options de conception à
                    choisir, aucun bras de héros est la même!
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Image Atelier et Phrase 3 -->
<div class="container overflow-hidden">
    <div class="row gy-4">
        <div class="col-sm-12 col-md-6">
            <div class="p-5 text-center text-light">
                <h4>Meet the Hero Arm – une prothèse de bras pour adultes et enfants</h4>
                <p class="p-4">Avancé, intuitif, robuste et léger. Le Hero Arm est le bras prothétique multi-grip le
                    plus abordable au monde, avec une fonctionnalité multi-grip et une esthétique puissante. Conçue et
                    fabriquée en Grande-Bretagne, la Hero Arm est une prothèse myoélectrique légère et abordable,
                    maintenant disponible dans plus de 801 endroits aux États-Unis pour les adultes amputés et les
                    enfants de 8 ans et plus. Votre différence de membre est votre superpouvoir.
                </p>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <img class="img-fluid rounded" src="asset/images/AtelierOpenBionics.jpg" alt="">
        </div>
    </div>
</div>

<?php $content=ob_get_clean();
require("view/template.php");