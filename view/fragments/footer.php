    <!-- Footer -->
    <footer class="container py-5 mt-auto">
        <div class="row text-center">
            <div class="col-sm-12 col-md-3">
                <p class="text-light text-decoration-none">© CARBON Jaison</p>
            </div>
            <div class="col-sm-12 col-md-3">
                <a class="text-light text-decoration-none" href="./?path=main&action=mensionslegales">Mensions légales</a>
            </div>
            <div class="col-sm-12 col-md-3">
                <a class="text-light text-decoration-none" href="./?path=main&action=contact">Contact</a>
            </div>
            <div class="col-sm-12 col-md-3">
                <a class="text-light text-decoration-none" href="./?path=main&action=apropos">Qui sommes nous ?</a>
            </div>
        </div>

    </footer>