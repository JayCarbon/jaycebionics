    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg">
        <div class="container-fluid">
            <a class="navbar-brand" href="./?path=main&action=accueil">
                <img src="asset/images/robotic-hand.png" alt="" width="70" height="70"
                    class="d-inline-block align-text-top">
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup"
                aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav mx-auto">
                    <a class="nav-link active" aria-current="page" href="./?path=main&action=accueil">Accueil</a>
                    <a class="nav-link" href="./?path=main&action=prothesebras">Prothèses pour Bras</a>
                    <a class="nav-link" href="./?path=main&action=prothesejambes">Porthèses pour Jambes</a>
                    <a class="nav-link" href="./?path=article&action=prothese">Configuration</a>
                    
                </div>
                <div class="text-light">
                    <?php if(!isset($_SESSION['email'])){
                            echo "<a class=\"btn btn-primary me-2\" href=\"?path=main&action=formLogin\">Connexion</a>";
                            echo "<a class=\"btn btn-success me-2\" href=\"?path=main&action=formInscription\">Inscription</a>";
                            }
                        elseif($_SESSION['idRole']=="2"){
                            echo "<span class=\"me-2\">Bonjour <b class=\"text-primary\">".$_SESSION['prenom'].' '.$_SESSION['nom']."</b></span>";
                            echo "<a class=\"btn btn-primary mx-2\" href=\"?path=admin&action=admin\">Menu Admin</a>";
                            echo "<a class=\"btn btn-danger\" href=\"?path=main&action=logout\">Déconnexion</a>";
                        }
                        else{
                            echo "<span class=\"me-2\">Bonjour <b class=\"text-primary\">".$_SESSION['prenom'].' '.$_SESSION['nom']."</b></span>";
                            echo "<a class=\"btn btn-primary mx-1\" href=\"?path=article&action=panier\">Panier</a>";
                            echo "<a class=\"btn btn-primary mx-2\" href=\"?path=article&action=valideCommande\">Commandes</a>";
                            echo "<a class=\"btn btn-danger\" href=\"?path=main&action=logout\">Déconnexion</a>";
                        } ?>
                </div>
            </div>
        </div>
    </nav>