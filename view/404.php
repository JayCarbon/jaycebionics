<?php $title = '404'; ?>

<?php ob_start(); ?>
<div class="container">
<h1 class="text-light">Erreur 404 : Oups ! La page que vous cherchez semble introuvable</h1>

<?php $content = ob_get_clean(); ?>

<?php require('template.php'); ?>