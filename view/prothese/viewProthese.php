<?php 
$title = 'Acheter Prothese'; 
ob_start(); 
   
?>

<div class="container text-light">
    <h1>Configuration</h1>
    <div class="row">
    <div class="col-lg-6 col-md-8 mx-auto">
            <img  id="imgProthese" src="asset/images/ajouterProtheses/1_1_6_1.jpeg" alt="" class="img-fluid rounded" style="width:50%;">
        </div>
        <form novalidate action="./?path=article&action=TestProthese" class="col-lg-6 col-md-8 mx-auto "
            enctype="multipart/form-data" method="post">
            <div>
            <label for="selectType">Type :</label>
            <select required name="idType" id="selectType" class="form-control">
            <option disabled selected value="">Choisir le type de prothèse</option>
            <?php 
            foreach($lesTypes as $unType)
            {
            echo ("<option value='".$unType->getIdType()."'>".$unType->getNom()."</option>");
            }
            ?>
            </select>
        </div>
        <div>
            <label for="selectModele">Modèle de la prothèse :</label>
            <select required name="idModele" id="selectModele" class="form-control">
            <option disabled selected value="">Choisir modèle de prothèse</option>
            <?php 
            foreach($lesModeles as $unModele)
            {
            echo ("<option value='".$unModele->getIdModele()."'>".$unModele->getNom()."</option>");
            }
            ?>
            </select>
        </div>
            <div>
            <label for="selectOrientation">Orientation de la prothèse :</label>
            <select required name="idOrientation" id="selectOrientation" class="form-control">
            <option disabled selected value="">Choisir l'orientation</option>
            <?php 
            foreach($lesOrientations as $uneOrientation)
            {
            echo ("<option value='".$uneOrientation->getIdOrientation()."'>".$uneOrientation->getOrientation()."</option>");
            }
            ?>
            </select>
        </div>
        <div>
            <label for="selectCouleur">Couleur de la prothèse :</label>
            <select required name="idCouleur" id="selectCouleur" class="form-control">
            <option  disabled selected value="">Choisir la couleur</option>
            <?php 
            foreach($lesCouleurs as $uneCouleur)
            {
            echo ("<option value='".$uneCouleur->getIdCouleur()."'>".$uneCouleur->getCouleur()."</option>");
            }
            ?>
            </select>
        </div>
        <h2 class="py-4">899 €</h2>
        <button class="btn btn-success my-2">Ajouter au panier</button>
        </form>

    </div>
</div>

<?php 
    $content = ob_get_clean(); 
    require("view/template.php");?>