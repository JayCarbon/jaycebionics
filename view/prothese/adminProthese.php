<?php

$title="Administration des protheses";

ob_start();?>
<div class="container text-light">
   <h1 class="text-center py-3">Administration des prothèses</h1>
   <h2><?php 
   if(isset($_SESSION['msg']))
   {
     echo $_SESSION['msg'];

     unset($_SESSION['msg']);
    
   }
   ?></h2>
   <table class="table table-striped text-light">
  <thead>
    <tr>
      <th scope="col">idProthese</th>
      <th scope="col">Modèle</th>
      <th scope="col">Orientation</th>
      <th scope="col">Couleur</th>
      <th scope="col">Image</th>
      <th scope="col">Prix</th>
      <th scope="col">Type</th>
      <th scope="col">Actions</th>
    </tr>
  </thead>
    <tbody>
        <?php
    foreach($protheses as $uneProthese)
{ 
  $laCouleur=$objetCouleurManager->fetchCouleurById($uneProthese->getIdCouleur());
  $leModele=$objetModeleManager->fetchModeleById($uneProthese->getIdModele());
  $lOrientation=$objetOrientationManager->fetchOrientationById($uneProthese->getIdOrientation());
?>    
<tr>
  <th scope="row" class="text-light"><?=$uneProthese->getIdProthese() ?></th>
  <td class="text-light"><?= $leModele->getNom() ?></td>
  <td class="text-light"><?= $lOrientation->getOrientation() ?></td>
  <td class="text-light"><?= $laCouleur->getCouleur()?></td>
  <td class="text-light"><img  id="imgProthese" src="asset/images/ajouterProtheses/<?=$uneProthese->getImage()?>" alt="" class="img-fluid rounded" style="width:3%;"></td>
  <td class="text-light"><?=$uneProthese->getPrix()?></td>
  <td class="text-light"><?=$uneProthese->getIdType()?></td>
  <td class="d-flex text-light">

      <a class="btn btn-warning mx-2 text-light" href="?path=admin&action=updateProthese&id=<?=$uneProthese->getIdProthese().'&idModele='.$uneProthese->getIdModele().'&idOrientation='.$uneProthese->getIdOrientation().'&idCouleur='.$uneProthese->getIdCouleur().'&image='.$uneProthese->getImage().'&prix='.$uneProthese->getPrix().'&idType='.$uneProthese->getIdType()?>">Modifier</a>
      
      <form action="?path=admin&action=traitementDeleteProthese" class="d-flex" method="post">
        <input type="hidden" name="id" id="inputId" required value=<?= $uneProthese->getIdProthese()?> >
        <button class="btn btn-danger mx-2">Supprimer</button>
      </form>
  </td>
</tr>

<?php
}
?>
</tbody>
</table>
<br>
</div>
<?php

$content= ob_get_clean();

require("view/template.php");
?>