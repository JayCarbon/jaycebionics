<?php
$title="Mofifier une prothese";

ob_start();?>

<div class="container text-light">

    <h1 class="d-flex justify-content-center">Formulaire de modification d'une prothese</h1>
    <form novalidate action="./?path=admin&action=traitementUpdateProthese" class="col-lg-6  col-md-8 mx-auto " enctype="multipart/form-data" method="post">
        <div>
            <input type="hidden" name="id" readonly value="<?=$id?>">
        </div>
        <div>
            <label for="selectModele">Modèle de la prothese :</label>
            <select required name="idModele" id="selectModele" class="form-control">
            <option selected>Choisir modèle de prothese</option>
            <?php 
            foreach($lesModeles as $unModele)
            {
            echo ("<option value='".$unModele->getIdModele()."'>".$unModele->getNom()."</option>");
            }
            ?>
            </select>
        </div>
        <div>
            <label for="selectOrientation">Orientation de la prothese :</label>
            <select required name="idOrientation" id="selectOrientation" class="form-control">
            <option selected>Choisir l'orientation</option>
            <?php 
            foreach($lesOrientations as $uneOrientation)
            {
            echo ("<option value='".$uneOrientation->getIdOrientation()."'>".$uneOrientation->getOrientation()."</option>");
            }
            ?>
            </select>
        </div>
        <div>
            <label for="selectCouleur">Couleur de la prothese :</label>
            <select required name="idCouleur" id="selectCouleur" class="form-control">
            <option selected>Choisir la couleur</option>
            <?php 
            foreach($lesCouleurs as $uneCouleur)
            {
            echo ("<option value='".$uneCouleur->getIdCouleur()."'>".$uneCouleur->getCouleur()."</option>");
            }
            ?>
            </select>
        </div>
        <div>
            <label for="inputImage">Image :</label>
            <input required type="text" name="image" id="inputImage" class="form-control" value=<?=$image?>>
        </div>
        <div>
            <label for="inputPrix">Prix de la prothese :</label>
            <input required type="number" name="prix" id="inputPrix" class="form-control" min="0" value=<?=$prix?>>
        </div>
        <div>
            <label for="selectType">Type :</label>
            <select required name="idType" id="selectType" class="form-control">
            <option selected>Choisir le type de prothese</option>
            <?php 
            foreach($lesTypes as $unType)
            {
            echo ("<option value='".$unType->getIdType()."'>".$unType->getNom()."</option>");
            }
            ?>
            </select>
        </div>
        <button class="btn btn-warning my-2">Modifier</button>
    </form>
</div>

<?php
$content= ob_get_clean();

require("view/template.php");
?>