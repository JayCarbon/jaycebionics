<?php
$title="Mofifier un type de prothèse";

ob_start();?>


<div class="container">

    <h1 class="d-flex justify-content-center text-light">Formulaire de modification d'un type de prothèse</h1>
    <!-- enctype si un fichier est envoyer avec le formulaire -->
    <form action="./?path=type&action=traitementUpdateType" class="col-lg-6  col-md-8 mx-auto " enctype="multipart/form-data" method="post">
        <div>
            <input type="hidden" name="id" readonly value="<?=$id?>">
            <label for="inputNomType">Nom du type de prothese :</label>
            <input required minlenght="6" type="text" name="nomType" id="inputNomType" class="form-control" value=<?=$nomType?>>
        </div>
        <button class="btn btn-warning my-2">Modifier</button>
    </form>
</div>

<?php
$content= ob_get_clean();

require("view/template.php");
?>