<?php
$title="Types";

ob_start();?>
<h1 class="text-light text-center py-3 col-12">Administration des types de prothèses</h1>
<!-- Affichage des messages d'erreurs et validation -->
<?php if (isset($_SESSION['erreur'])) {
        echo ("<div class='text-danger fw-bold text-center list-unstyled my-3'>");
        echo "<li>" . $_SESSION['erreur'] . "</li>";
        echo ("</div>");
        unset($_SESSION['erreur']);
    } elseif (isset($_SESSION['validation'])) {
        echo ("<div class='text-success fw-bold text-center list-unstyled my-3'>");
        echo "<li>" . $_SESSION['validation']. "</li>";
        echo ("</div>");
        unset($_SESSION['validation']);
    }
    ?>

<table class="table table-striped w-50 text-center mx-auto">
        <theader>
            <tr>
                <th class="text-light">ID du type</th>
                <th class="text-light">Nom du type</th>
                <th class="text-light">Actions</th>
            </tr>
        </theader>
        <tbody>
            <?php foreach($types as $type){
                ?>
            <tr>
                <td class="text-light"><?=$type->getIdType()?></td>
                <td class="text-light"><?=$type->getNom()?></td>
                <td class="text-light">
                    <a href="?path=type&action=updateType&id=<?=$type->getIdType().'&nomType='.$type->getNom()?>" class="text-light btn btn-warning col-6">Modifier</a>
                    <form action="?path=type&action=traitementDeleteType" method="post">
                        <input type="hidden" name="id" readonly value="<?=$type->getIdType()?>">
                        <button class="btn btn-danger col-6">Supprimer</button>
                    </form>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>

<?php
$content= ob_get_clean();

require("view/template.php");
?>