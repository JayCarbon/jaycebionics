<?php
$title="Ajouter un type de prothese";

ob_start();?>

    <h1 class="d-flex justify-content-center text-light py-5">Formulaire d'ajout d'un type de prothèse</h1>
    <form novalidate action="./?path=type&action=createTypeTraitement" class="col-lg-6  col-md-8 mx-auto text-light" enctype="multipart/form-data" method="post">
        <div>
            <label for="inputNomType">Nom du type de prothèse :</label>
            <input required type="text" name="nomType" id="inputNomType" class="form-control">
        </div>
        <button class="btn btn-success my-2">Ajouter</button>
    </form>
</div>

<?php
$content= ob_get_clean();

require("view/template.php");
?>