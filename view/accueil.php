<?php
$title="Accueil";
ob_start();?>

<!-- Main -->

<!-- Images des produits -->
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-6 mx-auto" style="position : relative;">
            <a href="./?path=article&action=prothese"><img class="img-fluid rounded" src="asset/images/HeroArm.jpg" alt="Responsive image"></a>
        </div>
        <div class="col-sm-12 col-md-6 mx-auto" style="position : relative;">
            <a href="./?path=article&action=prothese"><img class="img-fluid rounded" src="asset/images/CirclegZero.jpg" alt="Responsive image"></a>
        </div>
    </div>
</div>


<!-- Phrase 1 -->
<div class="container overflow-hidden">
    <div class="row gy-4">
        <div class="col-sm-12">
            <h1 class="p-5 text-sm-left text-center text-light">
                Imaginons un monde ou tout le monde à acces a des prothèses de haute calité
            </h1>
        </div>
    </div>
</div>


<!-- Images clients 1 -->
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <img class="img-fluid rounded" src="asset/images/ClientsOpenBionics.png" alt="Responsive image">
        </div>
    </div>
</div>


<!-- Phrase 2 -->
<div class="container overflow-hidden">
    <div class="row gy-4">
        <div class="col-sm-12">
            <div class="p-5 text-sm-justify text-justify text-light">
                <h3>35 à 40 millions de personnes dans le monde ont besoin de soins prothétiques ou orthétiques</h3>
                <p class="p-5">Aujourd’hui, seulement 1 personne sur 10 a accès à des produits d’assistance appropriés, y compris
                    des prothèses et des orthèses. Sans accès à de tels produits, de nombreuses personnes dans le besoin
                    sont confinées à leur domicile et vivent une vie de dépendance et d’exclusion, ce qui augmente
                    l’impact de la déficience et de l’incapacité sur la personne, la famille et la société.
                </p>
            </div>
        </div>
    </div>
</div>


<!-- Images clients 2 -->
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <img class="img-fluid rounded" src="asset/images/ClientsCircleg.jpg" alt="Responsive image">
        </div>
    </div>
</div>
<?php $content=ob_get_clean();
require("template.php");