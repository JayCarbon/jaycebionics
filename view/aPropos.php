<?php
$title="A propos";
ob_start();?>

<!-- Phrase Qui sommes nous -->
<div class="container overflow-hidden">
    <div class="row gy-4 align-items-center mx-auto">
        <!-- Image -->
        <div class="col-12">
            <img class="img-fluid rounded" src="asset/images/ImageQuiSommesNous.png" alt="Responsive image">
        </div>
        <!-- Phrase Qui sommes nous -->
        <div class="col-12 py-5">
            <h1 class="p-5 text-light text-center">Qui somes nous ?</h1>
            <p class="p-5 text-justify text-light bg-info rounded">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Perspiciatis sit molestias praesentium
                pariatur eveniet error quo ea itaque nulla! Enim numquam fugiat laborum odit consequatur hic eos
                eveniet soluta repellendus.
            </p>
        </div>
    </div>
</div>

<!-- Phrase 2 -->
<div class="p-5 container overflow-hidden">
    <div class="row gy-5 align-items-center justify-content-evenly">
        <div class="col-sm-3 col-md-3">
            <p class="p-4 text-center text-light bg-info rounded">
                Lorem
                <br><br>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
            </p>
        </div>
        <div class="col-sm-3 col-md-3">
            <p class="p-4 text-center text-light bg-info rounded">
                Lorem
                <br><br>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
            </p>
        </div>
        <div class="col-sm-3 col-md-3">
            <p class="p-4 text-center text-light bg-info rounded">
                Lorem
                <br><br>
                Lorem ipsum dolor sit amet consectetur adipisicing elit.
            </p>
        </div>
    </div>
</div>

<?php $content=ob_get_clean();
require("template.php");