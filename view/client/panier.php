<?php

/*TODO ET TOUJOURS VERIFIER LES CLASSES DE LA BDD LES COLONES ET LES VARCHAR*/

$title="Mon panier";

ob_start();?>
<div class="container text-light">
   <h1 class="py-4">Votre panier</h1>

   <table class="table table-striped text-light">
  <thead>
    <tr>
      <th scope="col">n°</th>
      <th scope="col">Modele</th>
      <th scope="col">Orientation</th>
      <th scope="col">Couleur</th>
      <th scope="col">Prix</th>
      <th scope="col"></th>
    </tr>
  </thead>
    <tbody>
        <?php
foreach($_SESSION["panier"] as $uneLigne)
{
?>    
<tr>

  <th scope="row" class="text-light"><?=$uneLigne["id"]?></th>
  <td class="text-light"><?=$uneLigne["nom"]?></td>
  <td class="text-light"><?=$uneLigne["couleur"]?></td>
  <td class="text-light"><?=$uneLigne["orientation"]?></td>
  <td class="text-light"><?=$uneLigne["prix"]?></td>
  <td class="text-light img-fluid rounded"> <img src="asset/images/ajouterProtheses/<?=$uneLigne["image"]?>" alt="responsive image" style="width:2%;"></td>
  <td class="d-flex text-light">

      <a class="btn btn-info text-light mx-2" href="./?path=article&action=valideCommande">Valider</a>
      
      <form action="?path=article&action=traitementDeleteProthesePanier" class="d-flex" method="post">
        <input type="hidden" name="id" id="inputId" required value=<?= $uneLigne["id"] ?> >
        <button class="btn btn-danger text-light mx-2">Supprimer</button>
      </form>
  </td>
</tr>
<?php
}
?>

</tbody>
</table>
<br>
</div>

<?php

$content= ob_get_clean();

require("view/template.php");
?>