<?php
$title="Inscription";
ob_start();?>

<!-- Main -->

<!-- Phrase 1 -->
<div class="container overflow-hidden">
    <div class="row gy-4">
        <div class="col-12">
            <h1 class="p-2 my-5 text-center text-light">INSCRIPTION</h1>
        </div>
    </div>
</div>

<!-- Affichage des messages d'erreurs et validation -->
<?php if (isset($_SESSION['erreur'])) {
        echo ("<div class='text-danger fw-bold text-center list-unstyled my-3'>");
        foreach ($_SESSION['erreur'] as $msgErreur) {
            echo "<li>" . $msgErreur . "</li>";
        }
        echo ("</div>");
        unset($_SESSION['erreur']);
    } elseif (isset($_SESSION['validation'])) {
        echo ("<div class='text-success fw-bold text-center list-unstyled my-3'>");
        foreach ($_SESSION['validation'] as $msgValidation) {
            echo "<li>" . $msgValidation . "</li>";
        }
        echo ("</div>");
        unset($_SESSION['validation']);
    }
    ?>

<!-- Formulaire d'inscription -->
<div class="container overflow-hidden">
    <div class="row gy-4">
        <div class="col-lg-6 col-md-8 mx-auto">
            <img class="bionichand bg-light p-2 rounded" src="asset/images/robotic-hand.png" alt="responsive image"
                style="width: 300px; height: 400px;">
        </div>
        <div class="col-lg-6 col-md-8 mx-auto text-light">
            <form novalidate action="./?path=main&action=traitementInscription" method="post">
                <div class="form-group">
                    <label for="InputNom">Nom</label>
                    <input type="text" class="form-control" id="InputNom" minlength="2" required name="nom" placeholder="Entrez votre nom">
                </div>
                <div class="form-group">
                    <label for="InputPrenom">Prenom</label>
                    <input type="text" class="form-control" id="InputPrenom" minlength="2" required name="prenom" placeholder="Entrez votre Prenom">
                </div>
                <div class="form-group">
                    <label for="InputEmail">Adresse e-amil</label>
                    <input type="email" class="form-control" id="InputEmail" required name="email" placeholder="example@email.com">
                </div>
                <div class="form-group">
                    <label for="InputAdresse">Adresse</label>
                    <input type="text" class="form-control" id="InputAdresse" minlength="8" required name="adresse" placeholder="Entrez votre adresse">
                </div>
                <div class="form-group">
                    <label for="InputVille">Ville</label>
                    <input type="text" class="form-control" id="InputVille" minlength="2" required name="ville" placeholder="Entrez votre ville">
                </div>
                <div class="form-group">
                    <label for="InputCodePostal">Code Postal</label>
                    <input type="number" class="form-control" id="InputCodePostal" minlength="4" required name="codepostal" placeholder="Entrez votre code costal">
                </div>
                <div class="form-group">
                    <label for="inputMDP1">Mot de passe</label>
                    <input type="password" class="form-control" id="inputMDP1" minlength="8" required name="mdp1" placeholder="Entrez votre mot de passe">
                </div>
                <div class="form-group">
                    <label for="inputMDP2">Confirmez votre mot de passe</label>
                    <input type="password" class="form-control" id="inputMDP2" minlength="8" required name="mdp2" placeholder="Confirmez votre mot de passe">
                </div>
                <div class="form-group">
                    <label for="Role"></label>
                    <input required type="hidden" class="form-control" id="inputRole" name="role" value="1">
                </div>
                <button type="submit" class="btn btn-primary my-4">S'inscrire</button>
            </form>
        </div>
    </div>
</div>
<?php $content=ob_get_clean();
require("view/template.php");