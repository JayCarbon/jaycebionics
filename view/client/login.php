<?php
$title="Login";
ob_start();?>

<!-- Main -->

<!-- Phrase 1 -->
<div class="container overflow-hidden">
    <div class="row gy-4">
        <div class="col-12">
            <h1 class="p-2 my-5 text-center text-light">CONNEXION</h1>
        </div>
    </div>
</div>

<!-- Affichage des messages d'erreurs et validation -->
<?php if (isset($_SESSION['erreur'])) {
        echo ("<div class='text-danger fw-bold text-center list-unstyled my-3'>");
        foreach ($_SESSION['erreur'] as $msgErreur) {
            echo "<li>" . $msgErreur . "</li>";
        }
        echo ("</div>");
        unset($_SESSION['erreur']);
    } elseif (isset($_SESSION['validation'])) {
        echo ("<div class='text-success fw-bold text-center list-unstyled my-3'>");
        foreach ($_SESSION['validation'] as $msgValidation) {
            echo "<li>" . $msgValidation . "</li>";
        }
        echo ("</div>");
        unset($_SESSION['validation']);
    }
    ?>

<!-- Formulaire de connexion -->
<div class="container overflow-hidden">
    <div class="row gy-4">
        <div class="col-lg-6 col-md-8 mx-auto">
            <img class="bionichand bg-light p-2 rounded" method="post" src="asset/images/robotic-hand.png" alt="responsive image"
                style="width: 300px; height: 400px;">
        </div>
        <div class="col-lg-6 col-md-8 mx-auto">
            <form method="post" action="./?path=main&action=traitementLogin">
                <div class="form-group">
                    <label for="inputMail">Adresse e-amil</label>
                    <input required minlenght="6" type="email" class="form-control" id="imputMail" name="email" placeholder="example@email.com">
                </div>
                <div class="form-group">
                    <label for="Mdp">Mot de passe</label>
                    <input required type="password" class="form-control" id="Mdp" name="mdp" placeholder="Entrez votre mot de passe">
                </div>
                <button type="submit" class="btn btn-primary my-4">Se connecter</button>
            </form>
        </div>
    </div>
</div>
<?php $content=ob_get_clean();
require("view/template.php");