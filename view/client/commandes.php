<?php
$title="Commandes";
ob_start();?>

<!-- Main -->

<!-- Phrase 1 -->
<div class="container overflow-hidden">
    <div class="row gy-4">
        <div class="col-12">
            <h1 class="p-2 my-5 text-center text-light">COMMANDES</h1>
        </div>
    </div>
</div>

<!-- Tableau de commandes -->
<table class="table table-striped table-dark w-50 text-center mx-auto">
    <thead>
        <tr>
            <th scope="col-3">Commande n°</th>
            <th scope="col-3">Date de commande</th>
            <th scope="col-3">Statut</th>
            <th scope="col-3">Date de livraison</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row">1</th>
            <td>JJ/MM/AAAA</td>
            <td>Livré</td>
            <td>JJ/MM/AAAA</td>
        </tr>
    </tbody>
</table>

<?php $content=ob_get_clean();
require("view/template.php");