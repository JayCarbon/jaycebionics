<?php
$title="Prothèses pour jambes";
ob_start();?>

<!-- Main -->

<!-- Images du produits -->
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <a href="./?path=article&action=prothese"><img class="img-fluid rounded" src="asset/images/HeaderCircleg.jpg" alt="Responsive image" style="position: relative;"></a>
        </div>
    </div>
</div>


<!-- Phrase 1 -->
<div class="container overflow-hidden">
    <div class="row gy-4">
        <div class="col-sm-12">
            <div class="p-5 text-center text-light">
                <h1 class="py-3">Une prothèse pour membres inférieurs pour les amputés au-dessous du genou</h1> 
                <p>Fabriqué à partir de plastique recyclé renforcé et certifié, créant des composants prothétiques légers, de haute qualité et confortables.
                </p> 
            </div>
        </div>
    </div>
</div>


<!-- Images clients juniors -->
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <img class="img-fluid rounded" src="asset/images/ClientsCircleg.jpg" alt="Responsive image">
        </div>
    </div>
</div>


<!-- Image sur Mesure et Phrase 2 -->
<div class="container overflow-hidden">
    <div class="row gy-4">
        <div class="col-sm-12 col-md-6">
            <img class="img-fluid rounded" src="asset/images/SurMesureCircleg.png" alt="">
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="p-5 text-center text-light">
                <h4>Modulaire et personnalisable</h4>
                <p class="p-5">Project Circleg garantit la personnalisation individuelle du système prothétique Circleg
                    grâce à sa conception modulaire. Cela permet de s’adapter à la taille du corps, au niveau d’activité
                    et aux préférences de couleur du bénéficiaire, tout en facilitant la maintenance et le remplacement
                    des pièces cassées.
                </p>
            </div>
        </div>
    </div>
</div>

<!-- Image Atelier et Phrase 3 -->
<div class="container overflow-hidden">
    <div class="row gy-4">
        <div class="col-sm-12 col-md-6">
            <div class="p-5 text-center text-light">
                <h4>Système modulaire</h4>
                <p class="p-4">Le système prothétique Circleg se compose d’un genou polycentrique, d’un pylône et d’un
                    pied dynamique. Ses composants peuvent être ajustés individuellement aux mesures du corps des
                    utilisateurs et sont connectés avec les adaptateurs pyramidaux bien connus.
                </p>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <img class="img-fluid rounded" src="asset/images/AtelierCircleg.jpg" alt="">
        </div>
    </div>
</div>
<?php $content=ob_get_clean();
require("view/template.php");