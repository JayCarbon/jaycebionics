<?php
$title="Mofifier l'utilisateur";

ob_start();?>

<div class="container text-light">

    <h1 class="d-flex justify-content-center">Formulaire de modification d'un utilisateur</h1>
    <!-- enctype si un fichier est envoyer avec le formulaire -->
    <form novalidate action="./?path=admin&action=updateUtilisateurTraitement" class="col-lg-6  col-md-8 mx-auto " enctype="multipart/form-data" method="post">
        <div>
            <input type="hidden" name="id" readonly value="<?=$id?>">
            <label for="inputNomUtilisateur">Nom :</label>
            <input required type="text" name="nom" id="inputNomUtilisateur" class="form-control" value=<?=$nom?>>
        </div>
        <div>
            <label for="inputPrenomUtilisateur">Prenom :</label>
            <input required type="text" name="prenom" id="inputPrenomUtilisateur" class="form-control" value=<?=$prenom?>>
        </div>
        <div>
            <label for="inputEmailUtilisateur">E-mail :</label>
            <input required type="text" name="email" id="inputEmailUtilisateur" class="form-control" value=<?=$email?>>
        </div>
        <div>
            <label for="inputMdpUtilisateur">Mot de passe :</label>
            <input required type="text" name="mdp" id="inputMdpUtilisateur" class="form-control" value=<?=$mdp?>>
        </div>
        <div>
            <label for="inputAdresseUtilisateur">Adresse :</label>
            <input required type="text" name="adresse" id="inputAdresseUtilisateur" class="form-control" value=<?=$adresse?>>
        </div>
        <div>
            <label for="inputVilleUtilisateur">Ville :</label>
            <input required type="text" name="ville" id="inputVilleUtilisateur" class="form-control" value=<?=$ville?>>
        </div>
        <div>
            <label for="inputCpUtilisateur">Code Postal :</label>
            <input required type="text" name="codePostal" id="inputCpUtilisateur" class="form-control" value=<?=$codePostal?>>
        </div>
        <div>
            <label for="inputIdRoleUtilisateur">idRole :</label>
            <input required type="text" name="idRole" id="inputIdRoleUtilisateur" class="form-control" value=<?=$idRole?>>
        </div>
        <button class="btn btn-warning my-2">Modifier</button>
    </form>
</div>

<?php
$content= ob_get_clean();

require("view/template.php");
?>