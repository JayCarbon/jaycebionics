<?php
$title="Commandes admin";
ob_start();?>
<!-- Main -->

<!-- Phrase 1 -->
<div class="container overflow-hidden">
    <div class="row gy-4">
        <div class="col-12">
            <h1 class="p-2 my-5 text-center text-light">COMMANDES ADMIN</h1>
        </div>
    </div>
</div>

<!-- Tableau de commandes admin -->
<table class="table table-striped table-dark col-12">
    <thead>
        <tr>
            <th scope="col-3">Commande n°</th>
            <th scope="col-3">Article</th>
            <th scope="col-3">Nom Client</th>
            <th scope="col-3">Prenom Client</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th scope="row">1</th>
            <td>Hero Arm</td>
            <td>Nom</td>
            <td>Prenom</td>
        </tr>
        <tr>
            <th scope="row">2</th>
            <td>Circleg</td>
            <td>Nom</td>
            <td>Prenom</td>
        </tr>
        <tr>
            <th scope="row">3</th>
            <td>Hero Arm</td>
            <td>Nom</td>
            <td>Prenom</td>
        </tr>
    </tbody>
</table>

<?php $content=ob_get_clean();
require("view/template.php");