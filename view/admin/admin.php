<?php $title = 'Admin'; ?>

<?php ob_start(); ?>
<div class="container text-light">
<h1 class="text-center py-5">Dashboard Admin</h1>

<div class="row">
    <div class="col-lg-4 col-md-5 col-12">
        <h2 class="text-light">Gestion des prothèses</h2>
        <a class="btn btn-info my-2 text-light" href="?path=admin&action=createProthese">Ajouter une prothèse</a>
        <br>
        <a class="btn btn-info my-2 text-light" href="?path=admin&action=adminProthese">Gèrer les prothèses</a>
    </div>

    <div class="col-lg-4 col-md-5 col-12">
        <h2>Gestion des types</h2>
        <a class="btn btn-info my-2 text-light" href="?path=type&action=createType">Ajouter un type de prothèse</a>
        <br>
        <a class="btn btn-info my-2 text-light" href="?path=type&action=type">Gèrer les types de prothèses</a>
    </div>

    <div class="col-lg-4 col-md-5 col-12">
        <h2>Gestion des utilisateurs</h2>
        <a class="btn btn-info my-2 text-light" href="?path=admin&action=gestionUtilisateur">Gèrer les utilisateurs</a>
        <br>
        <a class="btn btn-info my-2 text-light" href="#">Gèrer les commandes</a><!--COTE UTILISATEUR AFFICHAGE JUSTE ET ANNULATION COTE ADMIN AVEC VALIDATION DE LA COMMANDE-->
    </div>
</div>
<br>
</div>
<?php $content = ob_get_clean(); ?>

<?php require('view/template.php'); ?>