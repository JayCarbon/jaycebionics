<?php
$title="Gestion Utilisateur";

ob_start();?>

<h1 class="text-center py-3 text-light">Administration des utilisateurs</h1>

<table class="table table-striped w-50 text-center mx-auto">
        <theader>
            <tr>
                <th class="text-light">ID de l'utilisateur</th>
                <th class="text-light">Nom</th>
                <th class="text-light">Prenom</th>
                <th class="text-light">E-mail</th>
                <th class="text-light">Mot de passe</th>
                <th class="text-light">Adresse</th>
                <th class="text-light">Ville</th>
                <th class="text-light">CodePostal</th>
                <th class="text-light">Role de l'utilisateur</th>
                <th class="text-light">Actions</th>
            </tr>
        </theader>
        <tbody>
            <?php foreach($utilisateurs as $utilisateur){
                ?>

            <tr><!--FAIRE UN VAR DUMP D'UTILISATEURS--><!--SORT BY-->
                <td class="text-light"><?=$utilisateur->getIdUtilisateur()?></td>
                <td class="text-light"><?=$utilisateur->getNom()?></td>
                <td class="text-light"><?=$utilisateur->getPrenom()?></td>
                <td class="text-light"><?=$utilisateur->getEmail()?></td>
                <td class="text-light"><?=$utilisateur->getMdp()?></td>
                <td class="text-light"><?=$utilisateur->getAdresse()?></td>
                <td class="text-light"><?=$utilisateur->getVille()?></td>
                <td class="text-light"><?=$utilisateur->getCodePostal()?></td>
                <td class="text-light"><?=$utilisateur->getIdRole()?></td>
                <td class="text-light">
                    <a href="?path=admin&action=updateUtilisateur&id=<?=$utilisateur->getIdUtilisateur().'&nom='.$utilisateur->getNom().'&prenom='.$utilisateur->getPrenom().'&email='.$utilisateur->getEmail().'&mdp='.$utilisateur->getMdp().'&adresse='.$utilisateur->getAdresse().'&ville='.$utilisateur->getVille().'&codePostal='.$utilisateur->getCodePostal().'&idRole='.$utilisateur->getIdRole()?>" class="text-light btn btn-warning col-12">Modifier</a><!--&example meme valeur que dans name dans un formulaire et que la valeur d'un get ou d'un post dans un switch tout ce qui est dans l'url doit etre transféré a la page d'apres-->
                    <form action="?path=admin&action=deleteUtilisateurTraitement" method="post">
                        <input type="hidden" name="id" readonly value="<?=$utilisateur->getIdUtilisateur()?>">
                        <button class="btn btn-danger col-12">Supprimer</button>
                    </form>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>

<?php
$content= ob_get_clean();

require("view/template.php");